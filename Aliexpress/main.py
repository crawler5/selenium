import os
import time

from selenium import webdriver
import yaml


def load_username_password():
    yml_file = open(os.path.join('config/config.yaml'), 'r')
    Config = yaml.load(yml_file, Loader=yaml.FullLoader)
    yml_file.close()
    return Config["login"]["username"], Config["login"]["password"]


def frame_switch(css_selector):
    driver.switch_to.frame(driver.find_element_by_css_selector(css_selector))


def login(user_name, password):
    try:
        driver.find_element_by_xpath('//*[@id="batman-dialog-wrap"]/div/div/div[2]/div[1]/div/ul/li[2]/div').click()

        frame_switch("#alibaba-login-box")
        # enter the email
        time.sleep(2)
        driver.find_element_by_xpath('//*[@id="fm-login-id"]').send_keys(user_name)

        # enter the password
        time.sleep(2)
        driver.find_element_by_xpath('//*[@id="fm-login-password"]').send_keys(password)

        # click the login button
        time.sleep(2)
        driver.find_element_by_css_selector('#login-form > div.fm-btn > button').click()
    except Exception as e:
        print(e)


def execute_in_first_page():
    username, password = load_username_password()
    try:
        driver.find_element_by_css_selector('a.next-dialog-close').click()
    except Exception as e:
        pass
    driver.find_elements_by_css_selector('div.sku-property-image > img')[7].click()
    driver.find_elements_by_css_selector('button.buynow')[0].click()
    time.sleep(2)
    login(username, password)


def execute_in_others_page():
    username, password = load_username_password()
    login(username, password)
    try:
        driver.find_element_by_css_selector('a.next-dialog-close').click()
    except Exception as e:
        pass
    driver.find_elements_by_css_selector('div.sku-property-image > img')[7].click()
    driver.find_elements_by_css_selector('button.buynow')[0].click()


if __name__ == '__main__':
    driver = webdriver.Chrome(executable_path="data/windows/chromedriver.exe")

    driver.get("https://www.aliexpress.com/item/4001021635679.html")
    execute_in_first_page()
    count = 1
    for i in range(49):
        driver.execute_script("window.open('https://www.aliexpress.com/item/4001021635679.html')")
        print(driver.window_handles)
        driver.switch_to.window(driver.window_handles[i + 1])
        execute_in_others_page()
        count += 1
        time.sleep(1)
    print(count)
#    driver.quit()
