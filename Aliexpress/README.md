# Simulator user buy product with Selenium
* Page: Aliexpress 
* Step:
    1. Install chrome
        sudo apt-get install -y chromium-browser
    2. Install requirement
        pip install -r requirements.txt
    2. Edit username and password in config.yaml
    3. run main.py

Note: If you want close window after run program, you can uncomment this line #    driver.quit() in the end of file main.py
