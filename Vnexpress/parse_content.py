import datetime

from pyvirtualdisplay import Display
from selenium import webdriver
import urllib.parse
import pytz

zone = pytz.tzinfo("UTC+07:00")
urls = open("tmp/url.txt").readlines()

display = Display(visible=0, size=(800, 600))
display.start()
driver = webdriver.Firefox(executable_path="data/geckodriver")

# 2020-05-03T15:40:00.000+07:00
base_article = {
    'docId': None,
    'parentdocId': None,
    'eventId': None,
    'pageId': None,
    'userId': None,
    'docType': None,
    'type': None,
    'statusType': None,
    'sourceName': None,
    'sourceId': '',
    'sourceCategory': None,
    'rawContent': '',
    'categoryLevel1': '',
    'categoryLevel2': None,
    'category': None,
    'topic': None,
    'originalDate': None,
    'createDate': '',
    'updateDate': None,
    'shortformDate': '',
    'title': '',
    'summary': '',
    'content': '',
    'message': None,
    'name': None,
    'description': None,
    'caption': None,
    'story': None,
    'author': None,
    'authorId': None,
    'link': '',
    'parseLink': None,
    'links': None,
    'picture': '',
    'imageThumb': None,
    'images': None,
    'tag': None,
    'tags': None,
    'keywords': [],
    'actions': None,
    'numLikes': 0,
    'numReactions': 0,
    'numComments': 0,
    'numShares': 0,
    'numReacts': 0,
    'numView': 0,
    'hashcode': None,
    'signature': None,
    'groupId': None,
    'groupName': None,
    'rawArticle': None,
    'components': [],
    'newsArticle': None,
    'ids': None,
    'categories': None,
    'labels': None,
    'analyzeds': None,
    'scores': None,
    'entities': None,
    'collectDate': None
}


def parse_content(url):
    driver.get(url)
    json_article = base_article
    json_article["sourceId"] = urllib.parse.urlsplit(url).hostname
    json_article["title"] = driver.find_element_by_xpath('//*[@id="site-content"]/div[1]/article/header/h1').text
    json_article["summary"] = driver.find_element_by_xpath('//*[@id="site-content"]/div[1]/article/header/div[1]/div').text
    json_article["content"] = None
    json_article["link"] = url
    json_article["picture"] = driver.find_element_by_xpath("meta[property=og:image]").get_attribute("content")
    json_article["components"] = []
    json_article["collectDate"] = datetime.datetime.now().replace(tzinfo=datetime.tzinfo("UTC+07:00")).isoformat()
    json_article["createDate"] = driver.find_element_by_xpath("meta[property=article:published_time]").get_attribute("content")
    json_article["categoryLevel1"] = driver.find_element_by_xpath('//*[@id="site-content"]/div[1]/article/header/div[3]/a').text
    json_article["rawContent"] = ""
    json_article["shortformDate"] = datetime.datetime.strptime(json_article["collectDate"], "%Y-%m-%dT%H:%M:%S.%fz").date().strftime("%Y-%m-%d")



driver.quit()
display.stop()
