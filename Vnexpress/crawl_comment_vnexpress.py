import pandas as pd

from dateutil import parser
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


def convert_time(string_time):
    string_time = " ".join([i.strip() for i in string_time.split(",")[1:]])
    time = parser.parse(" ".join(string_time.split(" ")[:len(string_time.split(" ")) - 1]), dayfirst=True)
    time = time.strftime("%d-%m-%Y %H:%M")
    return time


def parse_content(url):
    display = Display(visible=0, size=(800, 600))
    display.start()
    driver = webdriver.Firefox(executable_path="data/geckodriver")
    driver.get(url)
    try:
        title = driver.find_element_by_xpath("//h1").text
        time = driver.find_element_by_xpath('//span[@class="date"]').text
        time = convert_time(time)
        current_url = driver.current_url
    except:
        print(url)
        return None
    while True:
        try:
            wait = WebDriverWait(driver, 10)
            wait.until(EC.invisibility_of_element_located((By.XPATH, '//div[@class="social-com"]')))
            button = driver.find_element_by_xpath('//*[@id="box_comment_vne"]/div/div[7]/a')
            button.click()
        except:
            return None

        try:
            buttons = driver.find_elements_by_xpath('//a[@class="view_all_reply"]')
            for button in buttons:
                button.click()

        except:
            pass
        try:
            users_tags = driver.find_elements_by_xpath('//a[@class="nickname"]')
            user_ids = []
            for tag in users_tags:
                link = tag.get_attribute("href")
                id = link.split("/")[-1]
                if id not in user_ids and id != 'javascript:;':
                    user_ids.append(id)
        except:
            return None
        try:
            subbutton = driver.find_element_by_xpath('//a[@class="btn-page next-page "]')
            subbutton.click()
        except:
            break
    driver.quit()
    display.stop()
    return title, time, user_ids, current_url


if __name__ == '__main__':
    cols = ["docid", "url", "title", "time", "uid"]

    doc_id = 0
    doc_id = 4102139
    for id in range(doc_id, 0, -1):
        result = []
        title, time, user_ids = "", "", []
        url = f"https://vnexpress.net/a-{id}.html"
        print(url)
        tmp = parse_content(url)
        if tmp is not None:
            title, time, user_ids, current_url = tmp
            for i in user_ids:
                result.append([id, current_url, title, time, i])
        if len(result) > 0:
            df = pd.DataFrame(result)
            df.columns = cols
            df.to_csv("tmp/output.csv", index=False, mode="a", header=False)
