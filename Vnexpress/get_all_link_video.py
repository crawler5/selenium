from pyvirtualdisplay import Display
from selenium import webdriver

display = Display(visible=0, size=(800, 600))
display.start()
driver = webdriver.Firefox(executable_path="data/geckodriver")


def parse_urls(url):
    print(url)
    links = []
    driver.get(url)
    for i in range(1000, 10000, 1000):
        driver.execute_script(f"window.scrollTo(0, {i});")

    tags = driver.find_elements_by_xpath('//*[@id="site-content"]//h2/a')
    for tag in tags:
        try:
            link = tag.get_attribute("href")
            links.append(link)
        except:
            continue
    print(len(links))
    return links


urls = [
    "https://baophapluat.vn/thpl-tin-tuc/",
    "https://baophapluat.vn/thpl-ong-kinh-phap-luat/",
    "https://baophapluat.vn/thpl-tieu-dung-va-du-luan/",
    "https://baophapluat.vn/thpl-dan-sinh/",
    "https://baophapluat.vn/thpl-bat-dong-san/",
    "https://baophapluat.vn/thpl-van-hoa-giai-tri/",
    "https://baophapluat.vn/thpl-ong-kinh-phap-luat/"
]

results = []
for url in urls:
    results += parse_urls(url)

driver.quit()
display.stop()
results = list(set(results))
f = open("tmp/url.txt", "a")
for i in results:
    f.write(i)
    f.write("\n")
