# Crawl vnexpress with Selenium
* Page: vnexpress 
* Step:
    1. Install Firefox 
    2. Run crawl_comment_vnexpress.py to get comment 
    3. Run parse_content.py to parse content 

Note: If you want close window after run program, you can uncomment this line #    driver.quit() in the end of file main.py
