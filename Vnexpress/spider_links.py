from pyvirtualdisplay import Display
from selenium import webdriver
from multiprocessing import Pool

base_url = "https://vnexpress.net"

cates = ["thoi-su", "goc-nhin", "the-gioi", "kinh-doanh", "giai-tri", "the-thao", "phap-luat",
         "giao-duc", "suc-khoe", "doi-song", "du-lich", "khoa-hoc", "so-hoa", "oto-xe-may",
         "y-kien", "tam-su", "cuoi"]


def parse_urls(url):
    print(url)
    links = []
    display = Display(visible=0, size=(800, 600))
    display.start()
    driver = webdriver.Firefox(executable_path="data/geckodriver")
    driver.get(url)
    for i in range(1000, 10000, 1000):
        driver.execute_script(f"window.scrollTo(0, {i});")

    tags = driver.find_elements_by_xpath("//a")
    for tag in tags:
        try:
            link = tag.get_attribute("href")
            if type(link) == str and "https://vnexpress.net" in link and ".html" in link:
                links.append(link)
        except:
            continue
    driver.quit()
    display.stop()
    print(len(links))
    return links


if __name__ == '__main__':
    links = []
    p = Pool(10)
    for cate in cates:
        urls = []
        for i in range(1, 1000):
            urls.append(f"{base_url}/{cate}-p{i}")

        links += p.map(parse_urls, urls)
        links = list(set(links))
        print(len(links))
        with open("tmp/data.txt", "a") as f:
            for link in links:
                if type(link) != str:
                    print(link)
                    for l in link:
                        f.write(l)
                        f.write("\n")
                    continue

                f.write(link)
                f.write("\n")
        links = []
